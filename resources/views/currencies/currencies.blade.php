@extends('layout')

@section('title', 'Currency market')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Currency list</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li class="active">
                    <a href="{{ route('currencies.index') }}"><strong>Currencies</strong></a>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="table-responsive">
                            @if(!$сurrencies->isEmpty())
                            <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                <thead>
                                <tr>
                                    <th class="hidden-xs">ID</th>
                                    <th>Logo</th>
                                    <th>Name</th>
                                    <th class="hidden-xs">Short name</th>
                                    <th class="hidden-xs">Price USD</th>
                                    <th class="text-right" data-sort-ignore="true">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($сurrencies as $currency)
                                    <tr>
                                        <td class="hidden-xs">
                                            {{$currency->id}}
                                        </td>
                                        <td>
                                            <img src="{{ $currency->logo_url}}" />
                                        </td>
                                        <td>
                                            <a href="{{ route('currencies.show', ['id'=>$currency->id])}}">{{$currency->title}}</a>
                                        </td>
                                        <td class="hidden-xs">
                                            {{$currency->short_name}}
                                        </td>
                                        <td class="hidden-xs">
                                            {{$currency->price}}
                                        </td>
                                        <td class="text-right">
                                            </a>
                                            <a href="{{route('currencies.edit', ['id'=>$currency->id])}}"
                                               class="btn btn-warning btn-sm">Edit
                                            </a>
                                            <form method="POST" action="{{route('currencies.destroy', ['id'=>$currency->id])}}">
                                                <input name="_method" type="hidden" value="DELETE">
                                                <input name="_token" type="hidden" value="{{csrf_token()}}">
                                                <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                            @else
                                <h2 class="center-block">No currencies</h2>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection