@extends('layout')

@section('title', "Edit $currency->title")

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Add currency</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li>
                    <a href="{{ route('currencies.index') }}">Currencies</a>
                </li>
                <li class="active">
                    <strong>Edit {{$currency->title}}</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" action="{{ route('currencies.update', ['id' => $currency->id]) }}" class="form-horizontal">
                        @method('PUT')
                        @csrf
                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}"><label class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-10"><input type="text" name="title" value="{{ old('title', $currency->title) }}" class="form-control">
                                @if($errors->has('title'))
                                    <span class="help-block m-b-none">{{ $errors->first('title') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('short_name') ? 'has-error' : '' }}"><label class="col-sm-2 control-label">Short name</label>
                            <div class="col-sm-10"><input type="text" name="short_name" value="{{ old('short_name', $currency->short_name) }}" class="form-control">
                                @if($errors->has('short_name'))
                                    <span class="help-block m-b-none">{{ $errors->first('short_name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('logo_url') ? 'has-error' : '' }}"><label class="col-sm-2 control-label">Logo URL</label>
                            <div class="col-sm-10"><input type="text" name="logo_url" value="{{ old('logo_url', $currency->logo_url) }}" class="form-control">
                                @if($errors->has('logo_url'))
                                    <span class="help-block m-b-none">{{ $errors->first('logo_url') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('price') ? 'has-error' : '' }}"><label class="col-sm-2 control-label">Price</label>
                            <div class="col-sm-10"><input type="text" name="price" value="{{ old('price', $currency->price) }}" class="form-control">
                                @if($errors->has('price'))
                                    <span class="help-block m-b-none">{{ $errors->first('price') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" type="submit">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection