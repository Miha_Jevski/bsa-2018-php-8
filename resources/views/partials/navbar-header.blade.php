<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-xs-8">
        <h1>Currency market</h1>
    </div>
    <div class="col-xs-4">
        <div class="pull-right m-t-md">
            <a href="{{ route('currencies.add') }} " class='btn btn-primary'>Add</a>
        </div>
    </div>
</div>